#include <stdio.h>

float mediac(float x, float y, float z){
    float media = (x+y+z)/3;
    return media;
}

int main(){
    float nota[3];
    float media;
    int controle = 1;
    printf("Digite as tres notas:\n");
    scanf("%f %f %f",&nota[0],&nota[1],&nota[2]);
    for(int i=0;i<3;i++){
        printf("A nota %d e: %.2f\n",i+1,nota[i]);
        if(nota[i]<0 || nota[i]>10){
            controle = 0;
        }
    }
    if(controle == 0){printf("Notas invalidas.");}
    else{
        media = mediac(nota[0],nota[1],nota[2]);
        printf("A media e: %.2f",media);
    }


}