#include <stdio.h>
#include <stdlib.h>

int maiores(int n, int *vet, int x) {
    int contador = 0;
    for(int i=0; i<n; i++) {
        if(vet[i]>x) {
            contador++;
        }
    }
    return contador;
}

void preenche_vetor(int n, int *vet) {
    float valor;
    for(int i = 0; i<n; i++) {
        printf("Digite o elemento %d do vetor: ", i);
        scanf("%d", &valor);
        vet[i] = valor;
        
    }
}

int main() {
    int n, x, quantidade;
    float maior_num;
    printf("Digite o tamanho do vetor: ");
    scanf("%d", &n);
    printf("Digite o número a ser comparado com os elementos do vetor: ");
    scanf("%d", &x);
    
    int *vet = (int*)malloc(sizeof(int)*n);
    
    preenche_vetor(n, vet);
    quantidade = maiores(n, vet, x);
    
    printf("%d", quantidade);
    
    return 0;
}