#include <stdio.h>
#include <stdlib.h>

float max_vet (int n, float *vet) {
    float maior_num = 0;
    for(int i = 0; i<n; i++) {
        if(vet[i] > maior_num) {
            maior_num = vet[i];
        }
    }
    return maior_num;
}

void preenche_vetor(int n, float *vet) {
    float valor;
    for(int i = 0; i<n; i++) {
        printf("Digite o elemento %d do vetor: ", i);
        scanf("%f", &valor);
        vet[i] = valor;
        
    }
}

int main() {
    int n;
    float maior_num;
    printf("Digite o tamanho do vetor: ");
    scanf("%d", &n);
    float *vet = (float*)malloc(sizeof(float)*n);
    
    preenche_vetor(n, vet);
    maior_num = max_vet(n, vet);
    printf("%f", maior_num);

    return 0;
}