#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int pegavalor(int numero){
    printf("Escolha um numero, de 0 a 100:\n");
    scanf("%d",&numero);
    return numero;
}

void acertou(int numero, int tentativa){
    printf("Voce acertou, parabens\nO numero sorteado foi: %d \nO numero de tentativas foi: %d",numero,tentativa);
}

int main(){
    srand(time(0));
    int sorteado;
    int numero = 0;
    int tentativa = 1;
    sorteado = rand()%101;
    numero = pegavalor(numero);
    if (numero==sorteado){
        acertou(numero,tentativa);
    }
    else{
        do{
            if(numero<sorteado){
                printf("Voce errou. O numero sorteado e maior do que a sua tentativa.\n");
                tentativa++;
                numero = pegavalor(numero);
            }
            else{
                printf("Voce errou. O numero sorteado e menor do que a sua tentativa.\n");
                tentativa++;
                numero = pegavalor(numero);
            }
        }
        while(numero!=sorteado);
        acertou(numero,tentativa);
    }
}