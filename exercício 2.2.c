#include <stdio.h>

int soma(int n){
    int j = 1;
    int soma = 0;
    for(int i=0;i<(n);i++){
        soma += j;
        j +=2;
    }
    return soma;
}

int main(){
    int n, k;
    printf("Digite quantos numeros naturais impares serao somados:\n");
    scanf("%d",&n);
    k = soma(n);
    printf("A soma dos %d numeros impares e: %d",n,k);
}