#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int indice(unsigned int numero, int i){
    i++;
    if (numero>9){
        i = indice(numero/10, i);
        return i;
    }
    else{
        return i;
    }
}

void printa(int *v,int i){
    int x = 0;
    int j = i;
    for(int k=0;k<i;k++){
        x = x * 10 + v[k];
        j--;
    }
    printf("%d",x);
}

int inverte(unsigned int numero, int i, int **v){
    i = indice(numero, i);
    int j = 0;
    int k = i;
    int *temp1 = (int*) malloc(i*sizeof(int));
    int *temp2 = (int*) malloc(i*sizeof(int));
    if(temp1 == NULL){
        printf("Sem memoria pro temp1");
        exit(1);
    }
    if(temp2 == NULL){
        printf("Sem memoria pro temp2");
        exit(1);
    }
    for(int j=0;j<i;j++){
        temp1[j]=numero/pow(10,k-1);
        numero -= temp1[j]*pow(10,k-1);
        if(j==i-1){
            temp1[j]+=1;
        }
        k--;
    }
    k=i-1;
    for(j=0;j<i;j++){
        temp2[j]=temp1[k];
        k--;
    }
    free(temp1);
    *v = temp2;
    return i;
}

int main(){
    unsigned int numero;
    int i=0;
    int *v = NULL;

    printf("Digite um numero:\n");
    scanf("%u",&numero);
    i=inverte(numero,i,&v);
    printa(v,i);
    free(v);
}