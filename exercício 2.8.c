#include <stdio.h>
#include <stdlib.h>

int calcula(int numero){
    int milhar = numero/1000;
    int centesimal = (numero/100)-(milhar*10);
    int decimal = (numero/10)-(centesimal*10)-(milhar*100);
    int unidade = numero-(decimal*10)-(centesimal*100)-(milhar*1000);
    
    int x1 = milhar*10 + centesimal;
    int x2 = decimal*10 + unidade;
    return x1 + x2;
}

void printavetor(int *v,int len){
    for(int i=0;i<len;i++){
        printf("%d ",v[i]);
    }
}

int tamanho(int len){
    for(int numero = 1000; numero<=9999;numero++){
        int soma = calcula(numero);
        if((soma*soma)==numero){
            len++;
        }
    }  
    return len;
}

void aloca(int **v, int len){
    int *temp = (int*) malloc(len*sizeof(int));
    if (temp == NULL){
        exit(1);
    }
    int i = 0;

    for(int numero = 1000; numero<=9999;numero++){
        int soma = calcula(numero);
        if(soma*soma==numero){
            temp[i]=numero;
            i++;
        }
    }
    *v = temp;    
}

int main(){
    int len = 0;
    int *v = NULL;
    len = tamanho(len);
    aloca(&v,len);

    printf("Existem %d numeros com a propriedade especificada. Sao eles:\n",len);
    printavetor(v,len);
    free(v);
}