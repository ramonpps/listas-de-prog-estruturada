#include <stdio.h>
#include <stdlib.h>

void printavetor(int *v, int len){
    for(int i=0;i<len;i++){
        printf("%d ",v[i]);
    }
}

void calcula(int x, int z, int len, int **v){
    int somador=x;
    int append=x;
    printf("Numero X: %d\n",x);
    printf("Numero Z: %d\n",z);
    while(x<z){ // com esse loop, conseguiremos o numero final (em x), e a quantidade de numeros que foi necessario somar (em len)
        somador++;
        len++;
        x+=somador;
    }
    int *temp = (int*) malloc(len*sizeof(int));
    if (temp==NULL){
        exit(1);
    }
    for(int j=0;j<len;j++){ // com esse for, conseguiremos resgatar o valor de cada soma que foi necessaria ate chegar no numero final
        append++;
        temp[j]=append;
    }
    printf("Resposta: %d\n",len);
    printf("Numero final: %d\n",x);
    printf("Numeros somados: ");
    *v = temp;
    printavetor(*v,len);
}

int main(){
    int x,z;
    int len = 0;
    int *v = NULL;
    printf("Digite um valor para x:\n");
    scanf("%d",&x);
    do{
        printf("Digite um valor para Z:\n");
        scanf("%d",&z);
        if(z<x){
            printf("Insira um valor para Z maior do que o valor de X.");
            fflush(stdin);
        }
    }
    while(z<x);
    calcula(x,z,len,&v);
    free(v);
}