#include <stdio.h>
#include <stdlib.h>

float media(int n, float *v) {
    float media = 0;
    for (int i=0; i<n; i++) {
        media += v[i];
    }
    media = media/n;
    return media;
}

void preenche_vetor(int n, float *v) {
    float valor;
    for(int i = 0; i<n; i++) {
        printf("Digite o elemento %d do vetor: ", i);
        scanf("%f", &valor);
        v[i] = valor;
        
    }
}

int main() {
    int n;
    float a;
    printf("Digite o tamanho do vetor: ");
    scanf("%d", &n);
    
    float *v = (float*)malloc(sizeof(float) * n);
    
    preenche_vetor(n, v);
    a = media(n, v);
    
    printf("%.2f", a);
    
    return 0;
}