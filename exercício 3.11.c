#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void preenche_dados(int n, int *idades, float *alturas, char *sexos) {
    int idade;
    float altura;
    char sexo;
    
    for (int i=0; i<n; i++) {
        printf("Digite a idade da pessoa %d: ", i+1);
        scanf("%d", &idade);
        printf("Digite a altura da pessoa %d: ", i+1);
        scanf("%f", &altura);
        printf("Digite o sexo (H/M) da pessoa %d: ", i+1);
        scanf(" %c",  &sexo);
        idades[i] = idade;
        alturas[i] = altura;
        sexos[i] = sexo;
    }
}

float calcula_variancia(int n, float *alturas) {
    float media = 0;
    float variancia = 0;
    
    for(int i=0; i<n; i++) {
        media += alturas[i];
    }
    media = media/n;
    
    for (int k = 0; k<n; k++) {
        variancia += pow(alturas[k] - media, 2);
    }
    variancia = variancia/n;
    return variancia;
}

void valores_de_interesse(int n, int *idades, float *alturas, char *sexos) {
    int contador_homens = 0;
    int contador_mulheres = 0;
    
    for(int i = 0; i<n; i++) {
        if ((sexos[i] == 'h' || sexos[i] == 'H') && alturas[i] > 1.8) {
            contador_homens += 1;
        }
        else if ((sexos[i] == 'm' || sexos[i] == 'M') && idades[i] > 20 && idades[i] < 35 ) {
            contador_mulheres += 1;
        }
    }
    printf("Homens com mais de 1.80m: %d\nMulheres com idade entre 20 e 25 anos: %d", contador_homens, contador_mulheres);
}

int main() {
    int n;
    float variancia;
    printf("Digite o número de pessoas: ");
    scanf("%d", &n);
    
    int *idades = (int*)malloc(sizeof(int) * n);
    float *alturas = (float*)malloc(sizeof(float) * n);
    char *sexos =  (char*)malloc(sizeof(char) * n);
    
    preenche_dados(n, idades, alturas, sexos);
    variancia = calcula_variancia(n, alturas);
    valores_de_interesse(n, idades, alturas, sexos);
    
    printf("\nvariância: %f", variancia);

    return 0;
}