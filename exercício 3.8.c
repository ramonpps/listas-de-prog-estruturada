#include <conio.h>
#include <stdio.h>

void main(){
	float vet[5] = {1.1,2.2,3.3,4.4,5.5};
	float *f;
	int i;
	f = vet;
	printf("contador/valor/valor/endereco/endereco");
	for(i = 0 ; i <= 4 ; i++){
		printf("\ni = %d",i);
		printf(" vet[%d] = %.1f",i, vet[i]);
		printf(" *(f + %d) = %.1f",i, *(f+i));
		printf(" &vet[%d] = %X",i, &vet[i]);
		printf(" (f + %d) = %X",i, f+i);
	}
}
//------------------------------------------------------------------------------------------
//contador/valor/valor/endereco/endereco

//i = 0 vet[0] = 1.1 *(f + 0) = 1.1 vet[0] = endereço do 1º elem do vetor (f + 0) = endereço do 1º elem do vetor

//i = 1 vet[0] = 2.2 *(f + 1) = 2.2 vet[1] = endereço do 2º elem do vetor (f + 1) = endereço do 2º elem do vetor

//i = 2 vet[2] = 3.3 *(f + 2) = 3.3 vet[2] = endereço do 3º elem do vetor (f + 2) = endereço do 3º elem do vetor

//i = 3 vet[3] = 4.4 *(f + 3) = 4.4 vet[3] = endereço do 4º elem do vetor (f + 3) = endereço do 4º elem do vetor

//i = 4 vet[4] = 5.5 *(f + 4) = 5.5 vet[4] = endereço do 5º elem do vetor (f + 4) = endereço do 5º elem do vetor

