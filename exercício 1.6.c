#include <stdio.h>
#include <math.h>

int main(){
    int a,b,c,d;
    float x1,x2;
    printf("Digite o termo A:\n");
    scanf("%d",&a);
    printf("Digite o termo B:\n");
    scanf("%d",&b);
    printf("Digite o termo C:\n");
    scanf("%d",&c);

    d = (b*b)-(4*a*c);
    if(d<0){
        printf("Nao ha raizes reais para a equacao %dx2+%dx+%d.\n",a,b,c);
    }
    else{
        x1 = (-b+sqrt(d))/(2*a);
        x2 = (-b-sqrt(d))/(2*a);
        printf("As raizes da equacao %dx2+%dx+%d sao: %.2f e %.2f.",a,b,c,x1,x2);
    }
}
