#include <stdlib.h>
#include <stdio.h>
#include <math.h>



void calc_circulo(float r, float *circunferencia, float * area) {  
  float pi = 3.14159265;
  *area =  pi*r*r;
	*circunferencia = 2*pi*r;
}

int main() {
  float r = 1;
  float circunferencia;
  float area;
  calc_circulo(r, &circunferencia, &area);
	printf("A circunferência tem %f uidades de medida, e a área tem %f unidades de área", circunferencia, area);

  return 0;
  
}