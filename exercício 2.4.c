#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(){
    char sorteado,letra;
    srand(time(NULL));
    //a função srand vincula a execução da rand a uma certa semente específica. Se for null, vai usar a semente do momento da execução do código, conferindo uma verdadeira aleatoriedade ao programa
    do{
        sorteado = rand();
    }
    while(sorteado<97||sorteado>122);
    //122 representa o z e 97 representa o A na tabela ascii
    printf("%c\n",sorteado);
    do{
        printf("Digite uma letra:\n");
        scanf("%c",&letra);
        if(letra==sorteado){
        printf("Voce acertou, parabens!");
        }
        else{
            printf("Errou.\n");
            fflush(stdin);
            //limpar o cache do input
        }
    }
    while(letra!=sorteado);
}