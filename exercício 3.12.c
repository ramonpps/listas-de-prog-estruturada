#include <stdio.h>
#include <conio.h>

int main() {
    char vetor[80];
    int i = 0;
    printf("Digite uma frase: ");
    while(vetor[i]!=13){
        vetor[i]=getche();
        i++;
    }
    printf("\nSua frase foi: ");
    for(int j = 0;j<i;j++){
        if(vetor[j]!='\0'){
            printf("%c",vetor[j]);
        }
        else{
            printf("\n");
        }
    }
}