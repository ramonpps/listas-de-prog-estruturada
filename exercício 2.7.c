#include <stdio.h>

int main(){
    int horas;
    float valor;
    printf("Digite o valor da hora trabalhada:\n");
    scanf("%f",&valor);
    printf("Digite o numero de horas trabalhadas:\n");
    scanf("%d",&horas);
    if (horas<=40){
        printf("O valor a ser recebido e de: R$ %.2f.",horas*valor);
    }
    if (horas>40&&horas<=60){
        printf("O valor a ser recebido e de: R$ %.2f.",(valor*40)+1.5*(horas-40)*valor);
    }
    if (horas>60){
        printf("O valor a ser recebido e de: R$ %.2f.",(valor*40)+(1.5*20*valor)+(2*(horas-60)*valor));
    }
}