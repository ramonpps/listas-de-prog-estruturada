#include <stdio.h>
#include <stdlib.h>

int calcula(int n, int *vetor){
    vetor[0] = 1;
    vetor[1] = 1;
    for(int i = 2;i<n;i++){
        vetor[i] = vetor[i-1]+vetor[i-2];
    }
    return n;
}

int main() {
    int n;
    printf("Digite a quantidade de termos da sequencia:\n");
    scanf("%d",&n);
    int *vetor = (int*) malloc(n*sizeof(int));
    if (vetor == NULL){
        exit(1);
    }
    calcula(n,vetor);
    printf("Os %d primeiros elementos da sequencia de fibonacci sao:\n",n);
    for(int i = 0;i<n;i++){
        printf("%d ",vetor[i]);
    }
    free(vetor);
}