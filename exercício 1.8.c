#include <stdio.h>

void converte(int x, int *a, int i){
    int temp1,temp2;
    temp1 = x%a[i];
    temp2 = x/a[i];

    printf("O numero de notas de %d e: %d\n",a[i],temp2);
    if(temp1!=0){
        i++;
        converte(temp1,a,i);
    }
}

int main(){
    int notas[7] = {100,50,20,10,5,2,1};
    int valor;
    int i = 0;
    printf("Digite um valor:\n");
    scanf("%d",&valor);
    converte(valor,notas,i);

}