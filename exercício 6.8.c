#include <stdio.h>
#include <stdlib.h>

int *repeticao(int *v1, int n1,int *n){
    int n2 = 0;
        for(int i=0;i<n1;i++){
        for(int j=i;j<n1-1;j++){
            if(v1[i]==v1[j+1]){
                n2 += 1;
            }
        }
    }
    int *v2 = (int*) malloc(n2*sizeof(int));
    if (v2 == NULL) {
        printf("Falha na alocação de memória");
        exit(1);
    }
    int k =0;
    for(int i=0;i<n1;i++){
        for(int j=i;j<n1-1;j++){
            if(v1[i]==v1[j+1]){
                v2[k] = v1[i];
                k++;
            }
        }
    }
    *n = n2;
    return v2;
}

int main(){
    int n1;
    printf("Digite o numero de elementos do vetor: ");
    scanf("%d",&n1);
    int *v1 = (int*) malloc(n1*sizeof(int));
    int n;
    if (v1 == NULL) {
        printf("Falha na alocação de memória");
        exit(1);
    }
    for(int i=0;i<n1;i++){
        printf("Digite o elemento %d do vetor: ",i+1);
        scanf("%d",&v1[i]);
    }

    int *v2 = repeticao(v1,n1,&n);
    printf("Os numeros repetidos sao:\n");
    for(int i=0;i<n;i++){
        printf("%d ",v2[i]);
    }
    free(v2);
    free(v1);
}