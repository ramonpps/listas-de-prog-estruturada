#include <stdio.h>
#include <stdlib.h>

float *acima_da_media(int n, float *vet, int *tam){
    int media = 0;
    for(int i=0;i<n;i++){
        media+=vet[i];
    }
    media = media/n;
    for(int i=0;i<n;i++){
        if(vet[i]>media){
            *tam+=1;
        }
    }
    float *k = (float*) malloc((*tam)*sizeof(float));
    if(k==NULL){
        printf("Falhou em alocar memória");
        exit(1);
    }
    int j=0;
    for(int i=0;i<n;i++){
        if(vet[i]>media){
            k[j]=vet[i];
            j++;
        }
    }
    return k;
}

int main(){
    int n;
    printf("Digite o numero de elementos do vetor: \n");
    scanf("%d",&n);
    float *v = (float*) malloc(n*sizeof(float));
    if(v==NULL){
        printf("Falhou em alocar memória");
        exit(1);
    }
    for(int i=0;i<n;i++){
        printf("Digite o valor do %d elemento: ",i+1);
        scanf("%f",&v[i]);
    }
    int tam = 0;
    float *k = acima_da_media(n,v,&tam);
    printf("[");
    for(int i=0;i<tam;i++){
        printf("%.2f ",k[i]);
    }
    printf("]");
    free(v);
    free(k);
}