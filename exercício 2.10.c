#include <stdio.h>

int mdc(int x, int y, int controle){
    int k;
    if(x<y){k=x;}else{k=y;}
    for(int i=2;i<=k;i++){
            while(x%i==0 && y%i==0){
                controle*=i;
                x = x/i;
                y = y/i;
        }
    }
    return controle;
}

int main(){
    int x,y;
    int controle=1;
    printf("Digite um numero:\n");
    scanf("%d",&x);
    printf("Digite outro numero:\n");
    scanf("%d",&y);
    controle = mdc(x,y,controle);
    printf("O mdc e: %d",controle);
}