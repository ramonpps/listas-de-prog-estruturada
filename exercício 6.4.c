#include <stdio.h>
#include <stdlib.h>

int *premiados(int n, int *inscr, float *t1, int p1, float *t2, int p2, int *tam) {
    float *medias = (float *)malloc(n * sizeof(float));
    if (medias == NULL) {
        printf("Falha na alocação de memória");
        exit(1);
    }

    for (int i = 0; i < n; i++) {
        float media = (t1[i] * p1 + t2[i] * p2) / (p1 + p2);
        medias[i] = media;
    }

    float maiorMedia = medias[0];
    for (int i = 1; i < n; i++) {
        if (medias[i] > maiorMedia) {
            maiorMedia = medias[i];
        }
    }

    int count = 0;
    for (int i = 0; i < n; i++) {
        if (medias[i] == maiorMedia) {
            count++;
        }
    }

    int *vencedores = (int *)malloc(count * sizeof(int));
    if (vencedores == NULL) {
        printf("Falha na alocação de memória");
        exit(1);
    }

    int index = 0;
    for (int i = 0; i < n; i++) {
        if (medias[i] == maiorMedia) {
            vencedores[index] = inscr[i];
            index++;
        }
    }

    *tam = count;
    free(medias);
    return vencedores;
}

int main() {
    int n, p1, p2;
    printf("Digite o número de participantes: ");
    scanf("%d", &n);

    printf("Digite o peso da primeira nota: ");
    scanf("%d", &p1);

    printf("Digite o peso da segunda nota: ");
    scanf("%d", &p2);

    int *inscr = (int *)malloc(n * sizeof(int));
    float *t1 = (float *)malloc(n * sizeof(float));
    float *t2 = (float *)malloc(n * sizeof(float));

    if (inscr == NULL || t1 == NULL || t2 == NULL) {
        printf("Falha na alocação de memória");
        exit(1);
    }

    for (int i = 0; i < n; i++) {
        printf("Digite a inscricao do participante %d: ", i + 1);
        scanf("%d", &inscr[i]);

        printf("Digite a nota 1 do participante %d: ", i + 1);
        scanf("%f", &t1[i]);

        printf("Digite a nota 2 do participante %d: ", i + 1);
        scanf("%f", &t2[i]);
    }

    int tam = 0;
    int *vencedores = premiados(n, inscr, t1, p1, t2, p2, &tam);

    printf("O participante premiados foi:\n");
    for (int i = 0; i < tam; i++) {
        printf("%d\n", vencedores[i]);
    }

    free(inscr);
    free(t1);
    free(t2);
    free(vencedores);

    return 0;
}