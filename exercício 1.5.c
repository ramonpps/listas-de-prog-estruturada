#include <stdio.h>

void c1(){
    float a = 3;
    float b = a/2;
    float c = b + 3.1;
    printf("Usando A, B e C como float, o valor de c e: %.1f\n",c);   
}

void c2(){
    int a = 3;
    float b = a/2;
    float c = b + 3.1;
    printf("Usando A como int e B e C como float, o valor de c e: %.1f\n",c);
}
void c3(){
    int a = 3;
    int b = a/2;
    int c = b + 3.1;
    printf("Usando A, B e C como int, o valor de c e: %d",c);
}

int main(){
    c1();
    c2();
    c3();
}