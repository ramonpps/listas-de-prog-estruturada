#include <stdio.h>
#include <stdlib.h>

int divs(int n, int* max, int* min) {
  int divisor = 2;
  int primo = 5;
  *max = 1;
  *min = 1;
  int verifica_menor_divisor = 0;
  
  while (n>1) {
    if (n%divisor==0 && divisor == n) {
      primo = 1;
      return primo;
    }
  
    if (n%divisor==0 && divisor > *max && divisor<n) {
      *max = divisor;
    }
    if (n%divisor==0 && divisor>1 && verifica_menor_divisor == 0) {
        printf("%d" , divisor);
        verifica_menor_divisor = 1;
        *min = divisor;
    }
    divisor++;
  }
  primo = 0;
  return primo;
}


int main() {  
  int n = 10;
  int max;
  int min;

  int primo = divs(n, &max, &min);

  printf("é primo: %d\nmaior divisor: %d\nmenor divisor: %d", primo, max, min);

  return 0;
}