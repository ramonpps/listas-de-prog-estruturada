#include <stdio.h>
#include <stdlib.h>

int *aprovados(int n, int *mat, float *notas, int *tam){
    for(int i=0;i<n;i++){
        if(notas[i]>5.0){
            *tam+=1;
        }
    }
    int *k = (int*) malloc(*tam*sizeof(int));
    if(k==NULL){
        printf("Falha na alocação de memória");
        exit(1);
    }
    int p = 0;
    for(int i=0;i<n;i++){
        if(notas[i]>=5.0){
            k[p]=mat[i];
            p++;
        }
    }
    return k;
}

int main(){
    int n;
    printf("Digite a quantidade de alunos da turma: \n");
    scanf("%d",&n);
    int *mat = (int*) malloc(n*sizeof(int));
    float *notas = (float*) malloc(n*sizeof(float));
    if(mat==NULL||notas==NULL){
        printf("Falha na alocação dos vetores");
        exit(1);
    }
    for(int i=0;i<n;i++){
        printf("Digite a matricula do aluno %d: ",i+1);
        scanf("%d",&mat[i]);
        printf("Digite a nota do aluno %d: ",i+1);
        scanf("%f",&notas[i]);
    }
    int tam = 0;
    int *a = aprovados(n,mat,notas,&tam);
    printf("As matriculas que passaram foram:\n");
    for(int i=0;i<tam;i++){
        printf("%d\n",a[i]);
    }
    free(mat);
    free(notas);
    free(a);
}