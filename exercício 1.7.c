#include <stdio.h>
#include <math.h>

int main(){
    float x[3];
    printf("Digite tres numeros float:\n");
    scanf("%f %f %f",&x[0],&x[1],&x[2]);
    for(int i =0;i<3;i++){
        printf("Numero %d arredondado para cima: %.2f.\n",i+1,ceil(x[i]));
        printf("Numero %d arredondado para baixo: %.2f.\n",i+1,floor(x[i]));
    }    
}